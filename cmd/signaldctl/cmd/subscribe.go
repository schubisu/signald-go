package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/cmd/signaldctl/config"
	client_protocol "gitlab.com/signald/signald-go/signald/client-protocol"
	v1 "gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

var (
	accountIdentifier string
	subscribeCmd      = &cobra.Command{
		Use:   "subscribe",
		Short: "subscribe to incoming messages from signald",
		Long: `subscribe to incoming messages from signald.

		if no default account is set, the -a/--account argument is required.

		the default output format (-o default) is being worked on, subject to change and should not be relied upon. Thoughts? come by the #signald IRC/matrix room

		if you want to future-proof your scripts, use json output (-o json) and parse it.`,
		PreRun: func(cmd *cobra.Command, args []string) {
			if accountIdentifier == "" {
				accountIdentifier = config.Config.DefaultAccount
			}
			if accountIdentifier == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			incoming := make(chan client_protocol.BasicResponse)
			go common.Signald.Listen(incoming)

			req := v1.SubscribeRequest{Account: accountIdentifier}
			err := req.Submit(common.Signald)
			if err != nil {
				panic(err)
			}

			for msg := range incoming {
				switch common.OutputFormat {
				case common.OutputFormatDefault:
					if msg.Type != "IncomingMessage" {
						continue
					}
					var data v1.IncomingMessage
					err := json.Unmarshal(msg.Data, &data)
					if err != nil {
						continue
					}

					if data.DataMessage == nil {
						continue
					}

					group := "-"
					if data.DataMessage.GroupV2 != nil {
						group = fmt.Sprintf("%s|%d", data.DataMessage.GroupV2.ID, data.DataMessage.GroupV2.Revision)
					}

					attachment := "-"
					if len(data.DataMessage.Attachments) > 0 {
						attachment = data.DataMessage.Attachments[0].StoredFilename
					}

					body := "-"
					if data.DataMessage.Body != "" {
						body = data.DataMessage.Body
					}
					fmt.Println(data.DataMessage.Timestamp, data.Account, data.Source.UUID, group, attachment, body)
				case common.OutputFormatJSON:
					err := json.NewEncoder(os.Stdout).Encode(msg)
					if err != nil {
						panic(err)
					}
				default:
					log.Fatal("unsupported output format")
				}
			}
			return nil
		},
	}
)

func init() {
	subscribeCmd.Flags().StringVarP(&accountIdentifier, "account", "a", "", "the signald account to use")
	RootCmd.AddCommand(subscribeCmd)
}

module gitlab.com/signald/signald-go

go 1.14

require (
	github.com/google/uuid v1.3.0
	github.com/jedib0t/go-pretty/v6 v6.1.0
	github.com/kr/text v0.2.0 // indirect
	github.com/lib/pq v1.10.4
	github.com/mattn/go-runewidth v0.0.10 // indirect
	github.com/mattn/go-sqlite3 v1.14.11
	github.com/mdp/qrterminal v1.0.1
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cobra v1.1.2
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/sys v0.0.0-20200519105757-fe76b779f299 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c // indirect
)
